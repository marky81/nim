Project Title
Playing around with the concept of a game called Nim. The concept is the player picks marbles, players can't pick more than half of the remaining marbles. The player who picks the last marble loses.

Getting Started
The game is a self contained python application, just download and run.

Prerequisites
You will need Python installed.

Authors
Mark Williams- Initial work

License
This project is licensed under the MIT License - see the LICENSE.md file for details

