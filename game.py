from math import *
from random import *

marbles = 0
player_one_state = False
player_two_state = False

def start():
    global marbles
    difficulty = raw_input("Easy or Hard: ")
    difficulty.lower()
    player_one_state = True
    if difficulty == "easy":
        marbles = randint(5,20)
        player_one_move()
    elif difficulty == "hard":
        marbles = randint(50,100)
        player_one_move()
    else:
        print "Hmm, you didn't pick anything."
        start()

def player_one_move():
    global marbles
    print "There are " + str(marbles)
    move = input("How many marbles would you like to take Player One?")
    gamestate(move)

def player_two_move():
    global marbles
    print "There are " + str(marbles)
    move = input("How many marbles would you like to take Player Two?")
    gamestate(move)

def winner(marbles):
    if player_one_state == True and marbles == 0:
        print "Player One has taken the last marble and lost!"
    elif player_two_state == True and marbles == 0:
        print "Player Two has taken the last marble and lost!"
    else:
        change_state()

def change_state():
    global player_one_state
    global player_two_state
    if player_one_state == True:
        player_one_state = False
        player_two_state = True
        player_two_move()
    else:
        player_two_state = False
        player_one_state = True
        player_one_move()

def gamestate(move):
    global marbles
    if marbles != 0:
        marbles = marbles - move
        winner(marbles)
    else:
        winner(marbles)

start()
